import React, {
  useState,
  useLayoutEffect,
  useCallback,
  useRef,
  useEffect,
} from "react"
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"
import {
  BackToHome,
  Home,
  Login,
  Register,
  Profile,
  LeftField,
  RightField,
  NotFound
} from "./components"
import {
  setIsPlaying,
  setIsGameOver,
  setNewGame,
} from "./createStore/reducers/gridSlice"
import "./styles/App.css"

const RequireAuth = ({ children, isAuth }) => {
  return isAuth ? children : <Navigate to="/login" />
}

const Tips = (props) => {
  return (
    <div className="modal">
      <div className="tip">
        Use "R" to rotate figure and arrow keys to move figure to left, right or
        down
      </div>
    </div>
  )
}
const EndGame = ({ userName, points }) => {
  return (
    <div className="modal">
      <div className="eg-header">GAME OVER</div>
      <div className="eg-points">
        {userName}, your points is {points}
      </div>
      <div className="eg-points">Press "q" key to play new game</div>
    </div>
  )
}
export const App = () => {
  const [startGame, setStartGame] = useState(false)
  const isAuth = useSelector((state) => state.auth.isAuth)
  const { points, isPlaying, isGameOver } = useSelector(
    (state) => state.gridState
  )
  const { userName } = useSelector((state) => state.userData)

  const dispatch = useDispatch()

  const isSingleGame = true

  const keyDown = useEvent((e) => {
    if (e.key === "Enter") {
      dispatch(setIsPlaying(true))
      !startGame && setStartGame(true)
    }
    if (isGameOver && e.key === "q") {
      dispatch(setIsGameOver(false))
      dispatch(setIsPlaying(false))
      dispatch(setNewGame(true))
      setStartGame(false)
    }
    return
  })

  function useEvent(handler) {
    const handlerRef = useRef(null)

    useLayoutEffect(() => {
      handlerRef.current = handler
    })

    return useCallback((...args) => {
      const fn = handlerRef.current
      return fn(...args)
    }, [])
  }

  useEffect(() => {
    window.addEventListener("keydown", keyDown)
    return () => {
      window.removeEventListener("keydown", keyDown)
    }
  }, [keyDown])

  return (
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<NotFound />} />
        <Route path="/" element={<Home isAuth={isAuth} />} />
        <Route
          path="/app"
          element={
            <RequireAuth
              className="wrapper"
              isAuth={isAuth}
              onKeyDown={keyDown}
            >
              <BackToHome />
              {startGame && !isPlaying ? <Tips /> : ""}
              <div className="App">
                <div
                  className="start"
                  style={startGame ? { opacity: "0" } : { opacity: "1" }}
                >
                  Press "ENTER" to start the game and "Space" to pause
                </div>
                <LeftField
                  style={isSingleGame ? {} : { borderRight: "2px white solid" }}
                />
                {isSingleGame ? "" : <RightField />}
                {isGameOver && (
                  <EndGame
                    userName={userName}
                    points={points}
                    onKeyDown={keyDown}
                  />
                )}
              </div>
            </RequireAuth>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route
          path="/profile"
          element={
            <RequireAuth className="wrapper" isAuth={isAuth}>
              <Profile />
            </RequireAuth>
          }
        />
      </Routes>
    </BrowserRouter>
  )
}
