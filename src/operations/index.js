export { blockStateCheck } from "./blockStateCheck"
export { keyCheck } from "./keyCheck"
export { createBlock } from "./createBlock"
export { blockInsert } from "./blockInsert"
export { blockRender } from "./blockRender"
export { checkUnderBlock } from "./checkUnderBlock"
export { horizontalCheck } from "./horizontalCheck"
export { findTopLeft } from "./findTopLeft"
export { verticalCheck } from "./verticalCheck"
export { clear } from "./clear"
export { rotate } from "./rotate"
export { randomize } from "./randomize"
export { formInfo } from "./formInfo"

// const rotatedTetrimino = [];
//   const w = newState.currentBlock.length;
//   const h = newState.currentBlock[0].length;
//   const tetrimino = newState.currentBlock;

//   for (let i = 0; i < h; i++) {
//     const row = [];
//     for (let j = 0; j < w; j++) {
//       row.push(tetrimino[w - (w - j - 1) - 1][h - i - 1]);
//     }
//     rotatedTetrimino.push(row);
//   }