import { randomize } from "./randomize";
import blocks from "../createStore/blocks";

export function createBlock(grid/*copyGridState*/, block) {

    block.pattern = blocks[block.next] || blocks[randomize()];
    block.isBlock = true;
    block.currentLoc = [3, 0];
    const [x, y] = block.currentLoc;
    const xSize = block.pattern[0].length;
    const ySize = block.pattern.length;

    if (grid[0][3] > 0 && grid[0][3] < 10) {
        grid[0][0] = -3
    }
    
    for( let i = 0; i < ySize; i++) {
        if(i + y < 0) continue; 
        for (let j = 0; j < xSize; j++) {        
            grid[i+y][j+x] = block.pattern[i][j];
        }
    }

    block.next = randomize();

    return grid;
}