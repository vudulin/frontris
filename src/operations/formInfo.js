export function formInfo(playerId, avatar, points, next) {
    return {
        playerId : playerId,
        avatar: avatar,
        points: points,
        next: next,
    }
}