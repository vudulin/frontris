export function checkUnderBlock(cgs, block, nx, ny) {
    
    const [x, y] = block.currentLoc
    const xSize = block.pattern[0].length    
    const ySize = block.pattern.length    

    for(let i = 0; i < ySize; i++) {
        for(let j = 0; j < xSize; j++) {
            const nc = cgs[i+ny][j+x] 
            const c = cgs[i+y][j+x]
            //if true then moving is not blocked
            
            if(nc <= 0) continue
            if(nc > 0 && c < 10) continue
            if(c === nc) continue
            if(c === -1) continue
             
            return true
        }
    }
    return false
}