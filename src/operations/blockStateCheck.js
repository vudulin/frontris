import * as o from "."

export function blockStateCheck(grid, block, key) {

    const offset = createOffset(key)
    
    let copyGrid = grid.map((r) => r.slice(0))

    let newBlock = {
        currentLoc: block.currentLoc,
        isBlock: block.isBlock,
        pattern: block.pattern, 
        next: block.next,
    }
    
    if(!newBlock.isBlock) {   
        copyGrid = o.createBlock(copyGrid, newBlock);

        return [copyGrid, newBlock, 0]
    } //Creating block

    if(key === "r") {   
        newBlock.pattern = o.rotate(newBlock, copyGrid)
    }   

    const [x, y] = newBlock.currentLoc;
    const newLoc = [o.horizontalCheck(copyGrid, newBlock, offset[0]), y + offset[1]]
    const ySize = newBlock.pattern.length

    let countLines = 0 
   
    if(offset[1] === 1 && y + ySize + 1 > copyGrid.length) {
        [copyGrid, countLines] = o.blockInsert(copyGrid, newBlock)    

        return [copyGrid, newBlock, countLines];
    } //Checking for bottom boundary

    if(offset[1] === 1 && o.checkUnderBlock(copyGrid, newBlock, ...newLoc)) {
        [copyGrid, countLines] = o.blockInsert(copyGrid, newBlock)  

        return [copyGrid, newBlock, countLines];
    } //Checking if there are blocks under falling block
    
    copyGrid = o.blockRender(copyGrid, newBlock, ...newLoc)

    return [copyGrid, newBlock, countLines];
}

function createOffset(key) {
    if(key === "ArrowDown") return [0, 1]
    if(key === "ArrowLeft") return [-1, 0]
    if(key === "ArrowRight") return [1, 0]
    return [0,0]
}