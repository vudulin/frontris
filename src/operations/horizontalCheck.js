export function horizontalCheck(grid, block, os) {
    const [x, y] = block.currentLoc 
    const xSize = block.pattern[0].length
    const ySize = block.pattern.length
    
    if(os > 0) {
        if(x + xSize >= 10) return x
        for(let i = y; i < y+ySize; i++) {
            for (let j = x; j < x+xSize; j++) {
                const nc = grid[i][j+1]
                const c = block.pattern[i-y][j-x]

                if(nc <= 0) continue
                if(nc > 0 && c < 0) continue
                if(c === nc) continue
                if(c === -1) continue

                return x
            }
        }
    }

    if(os < 0) {
        if(x === 0) return x
        for(let i = y; i < y+ySize; i++) {
            for (let j = x; j < x+xSize; j++) {
                const nc = grid[i][j-1]
                const c = grid[i][j]
                
                if(nc <= 0) continue
                if(nc > 0 && c < 0) continue
                if(c === nc) continue
                if(c === -1) continue

                return x
            }
        }
    }
    return x + os
}