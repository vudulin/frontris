import { clear } from "./clear";

export function blockRender(cgs, block, nx, ny) {

    const xSize = block.pattern[0].length
    const ySize = block.pattern.length    
    cgs = clear(cgs)
    for( let i = 0; i < ySize; i++) {
        for (let j = 0; j < xSize; j++) {           
            
            cgs[i+ny][j+nx] = cgs[i+ny][j+nx] > 0 ? cgs[i+ny][j+nx] :
            block.pattern[i][j]
        }
    }

    block.currentLoc = [nx, ny]
    
    return cgs
}