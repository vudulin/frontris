export function clear(grid) {
    return grid.map((r) => r.map((c) => {
        return c = (c > 10 || c === -1) ? 0 : c
    }))
}