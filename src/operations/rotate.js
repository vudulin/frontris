export function rotate(block, grid) {

  let [x, y] = block.currentLoc

  let newRotateLoc = (x + block.pattern[0].length === 10 && block.pattern[0].length - block.pattern.length < 0) ? 
      [x + block.pattern[0].length - block.pattern.length, y] :
      block.currentLoc;
      
  [x, y] = newRotateLoc

  for(let i = 0; i < block.pattern[0].length; i++) {
      for(let j = 0; j < block.pattern.length; j++) {
          if(grid[i + y][j + x] > 0 && grid[i + y][j + x] < 10) return block.pattern
      }
  }   

  block.currentLoc = [x, y];

  const rotatedBlock = [];
  const w = block.pattern.length;
  const h = block.pattern[0].length;
  const pattern = block.pattern;

  for (let i = 0; i < h; i++) {
    const row = [];
    for (let j = 0; j < w; j++) {
      row.push(pattern[w - (w - j - 1) - 1][h - i - 1]);
    }
    rotatedBlock.push(row);
  }

    return rotatedBlock
}