export function verticalCheck(cgs, block, os) {
    const size = block.pattern.length
    const [x, y] = block.currentLoc
    const pattern = block.pattern
    
    if(y + size === 21) return false

    for(let i = pattern.length-1; i >= 0; i--) {
        for(let j = 0; j < pattern[i].length; j++) {            
            if(cgs[y+i+1][x+j] < 0 ||
               cgs[y+i][x+j] === cgs[y+i+1][x+j]) return true
        }
    }
    
    return true
}