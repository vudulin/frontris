const KEYS = ["ArrowDown", "ArrowLeft", "ArrowRight", "r"]

export function keyCheck(key) {
  return KEYS.findIndex((e) => e === key) === -1 ? false : true
}
