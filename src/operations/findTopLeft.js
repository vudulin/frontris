export function findTopLeft(cgs, block) {
    for(let i = 0; i < cgs.length; i++) {
        for( let j = 0; j < cgs[i].length; j++) {
            if(cgs[i][j] > 10 || cgs[i][j] === -1) {                
                return [j, i]
            }
        }
    }
    return block.currentLoc
}