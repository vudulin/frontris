import { clear } from "./clear";

export function blockInsert(copyGridState, block) {
    copyGridState = copyGridState.map(r => r.map(c => c > 10 ? c-10 : c))
    block.currentLoc = [3,0]
    block.isBlock = false;

    let newGrid = copyGridState
    let countLines = 0
    for(let i = 0; i < newGrid.length; i++) {
        if(newGrid[i].every((e) => e > 0)) {
            newGrid.splice(i, 1)
            newGrid.unshift([0,0,0,0,0,0,0,0,0,0])
            countLines++
        } 
    }

    return [copyGridState, countLines]
}