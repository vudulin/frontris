import { createSlice } from "@reduxjs/toolkit"

export const userDataSlice = createSlice({
  name: "userData",
  initialState: {
    userName: "",
    email: "",
    avatarUrl: "",
    id: "",
    createdAt: "",
    updatedAt: "",
  },
  reducers: {
    setUserData: (state, action) => {
      return (state = action.payload)
    },
  },
})

export const { setUserData } = userDataSlice.actions

export default userDataSlice.reducer
