import { combineReducers } from "@reduxjs/toolkit"
import gridSliceReducer from "./gridSlice"
import authSliceReducer from "./authSlice"
import userDataSliceReducer from "./userDataSlice"
import tokenReducer from "./tokenSlice"

export default combineReducers({
  gridState: gridSliceReducer,
  auth: authSliceReducer,
  userData: userDataSliceReducer,
  token: tokenReducer,
})
