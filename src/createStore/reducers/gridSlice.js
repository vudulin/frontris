import { createSlice } from "@reduxjs/toolkit"

export const gridSlice = createSlice({
  name: "gridState",
  initialState: {
    points: 0,
    next: null,
    isPlaying: false,
    isGameOver: false,
    newGame: false,
  },
  reducers: {
    setPoints: (state, action) => {
      state.points = action.payload
    },
    setNext: (state, action) => {
      state.next = action.payload
    },
    setIsPlaying: (state, action) => {
      state.isPlaying = action.payload
    },
    setIsGameOver: (state, action) => {
      state.isGameOver = action.payload
    },
    setNewGame: (state, action) => {
      state.newGame = action.payload
    },
  },
})

export const { setPoints, setNext, setIsPlaying, setIsGameOver, setNewGame } =
  gridSlice.actions

export default gridSlice.reducer
