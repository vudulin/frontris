import React, { useState } from "react"
import { Link, Navigate } from "react-router-dom"
import axios from "axios"
import { useDispatch, useSelector } from "react-redux"
import { setIsAuth } from "../createStore/reducers/authSlice"
import { setUserData } from "../createStore/reducers/userDataSlice"
import { setToken } from "../createStore/reducers/tokenSlice"
import { BackToHome } from "./"
import "../styles/Register.scss"

const Check = ({ color }) => {
  return (
    <svg
      className="spinner"
      width="30px"
      height="30px"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <rect
        className="spinner__rect"
        x="0"
        y="0"
        width="100"
        height="100"
        fill="none"
      ></rect>
      <circle
        className="spinner__circle"
        cx="50"
        cy="50"
        r="40"
        stroke={color}
        fill="none"
        strokeWidth="6"
        strokeLinecap="round"
      ></circle>
    </svg>
  )
}

const Success = () => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 130.2 130.2"
    >
      <circle
        className="path circle"
        fill="none"
        stroke="#73AF55"
        strokeWidth="6"
        strokeMiterlimit="10"
        cx="65.1"
        cy="65.1"
        r="62.1"
      />
      <polyline
        className="path check"
        fill="none"
        stroke="#73AF55"
        strokeWidth="6"
        strokeLinecap="round"
        strokeMiterlimit="10"
        points="100.2,40.2 51.5,88.8 29.8,67.5 "
      />
    </svg>
  )
}

const Reject = () => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 130.2 130.2"
    >
      <circle
        className="path circle"
        fill="none"
        stroke="#D06079"
        strokeWidth="6"
        strokeMiterlimit="10"
        cx="65.1"
        cy="65.1"
        r="62.1"
      />
      <line
        className="path line"
        fill="none"
        stroke="#D06079"
        strokeWidth="6"
        strokeLinecap="round"
        strokeMiterlimit="10"
        x1="34.4"
        y1="37.9"
        x2="95.8"
        y2="92.3"
      />
      <line
        className="path line"
        fill="none"
        stroke="#D06079"
        strokeWidth="6"
        strokeLinecap="round"
        strokeMiterlimit="10"
        x1="95.8"
        y1="38"
        x2="34.4"
        y2="92.2"
      />
    </svg>
  )
}

const RegisterForm = () => {
  const [validData, setValidData] = useState({
    userName: "",
    email: "",
    password: "",
  })
  const [inputData, setInputData] = useState({
    userName: "",
    email: "",
    password: "",
  })
  const [submitPending, setSubmitPending] = useState(null)

  const [nameIsFree, setNameIsFree] = useState(null)
  const [pending, setPending] = useState(null)

  const [isUsernameError, setIsUsernameError] = useState(false)
  const [isEmailError, setIsEmailError] = useState(false)
  const [isPasswordError, setIsPasswordError] = useState(false)
  const [isRegisteredError, setIsRegisteredError] = useState("")

  const isAuth = useSelector((state) => state.auth.isAuth)

  const dispatch = useDispatch()

  const setAuth = () => {
    if (!isAuth) dispatch(setIsAuth(!isAuth))
  }

  const handleChangeData = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    const string = name === "email" ? value.toLowerCase() : value
    setInputData((state) => {
      return {
        ...state,
        [name]: string,
      }
    })
    if (name === "userName" && string.length >= 3) checkNameRequest(string)
    if (validate(name, string)) {
      setValidData((state) => {
        return {
          ...state,
          [name]: string,
        }
      })
    }
    if (validData[name] !== ("" && inputData[name])) validData[name] = ""
  }

  const validate = (typeOfData, input) => {
    const validationRegex = {
      userName: /^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/,
      email:
        /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/,
      password: /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/,
    }
    return validationRegex[typeOfData].test(input)
  }

  const checkNameRequest = async (name) => {
    setIsUsernameError(false)
    setPending(true)
    await axios
      .get(`https://backtris.vercel.app/auth/check/${name}`)
      .then((res) => {
        setPending(false)
        if (!res) return
        if (res.data.message === "ok") return setNameIsFree(true)
        setNameIsFree(false)
        setIsUsernameError(true)
      })
      .catch((error) => {
        setPending(null)
        setNameIsFree(null)
        setIsUsernameError(true)
        console.log("error -", error.message)
      })
  }

  const registerRequest = async () => {
    const { userName, email, password } = validData
    await axios
      .post("https://backtris.vercel.app/auth/register", {
        userName,
        email,
        password,
      })
      .then((res) => {
        if (res) {
          setIsUsernameError((state) => (state = false))
          setIsEmailError((state) => (state = false))
          setIsPasswordError((state) => (state = false))
          setAuth(!isAuth)
          setSubmitPending(false)
          dispatch(setUserData(res.data.userData))
          dispatch(setToken(res.data.token))
        }
      })
      .catch((error) => {
        setIsUsernameError((state) => (state = true))
        setIsEmailError((state) => (state = true))
        setIsPasswordError((state) => (state = true))
        setSubmitPending(false)
        setIsRegisteredError(
          (state) =>
            (state =
              "You've already registered. Change your username, email and password to create new account")
        )
        console.log("error - ", error.message)
      })
  }

  const submitForm = (e) => {
    e.preventDefault()
    setSubmitPending(true)
    registerRequest()
  }

  const onKeyDownEnter = (e) => {
    const { email, userName, password } = validData
    if (e.key.toLowerCase() === "enter") {
      const form = e.target.form
      const index = [...form].indexOf(e.target)
      email !== "" && password !== "" && submitForm(e)
      if (email && userName && password) return
      index === form.elements.length - 1
        ? form.elements[0].focus()
        : form.elements[index + 1].focus()
    }
    return
  }

  return (
    <div className="form">
      <form className="login__form" onKeyDown={onKeyDownEnter}>
        <p className="login__title">Registration</p>
        <input
          className="login-input"
          type="text"
          required=""
          placeholder="Username"
          name="userName"
          onChange={handleChangeData}
        />
        <div
          className="user-input-animation"
          style={
            (pending === null && nameIsFree === null) ||
            inputData.userName.length < 3
              ? { opacity: 0 }
              : { opacity: 1 }
          }
        >
          {pending ? (
            <Check color={"#f5f243"} />
          ) : nameIsFree ? (
            <Success />
          ) : (
            <Reject />
          )}
        </div>
        <p
          className="tooltip"
          style={isUsernameError ? { opacity: 1 } : { opacity: 0 }}
        >
          {isUsernameError && "Username has already taken"}
        </p>
        <input
          className="login-input"
          type="email"
          required=""
          placeholder="Email"
          name="email"
          onChange={handleChangeData}
        />
        <p
          className="tooltip"
          style={isEmailError ? { opacity: 1 } : { opacity: 0 }}
        >
          Please enter a valid email address, e.g. user@user.com
        </p>
        <input
          className="login-input"
          type="password"
          required=""
          placeholder="Password"
          name="password"
          onChange={handleChangeData}
        />
        <p
          className="tooltip"
          style={isPasswordError ? { opacity: 1 } : { opacity: 0 }}
        >
          Your password must be between 8 and 16 characters long, with no
          spaces, and contain at least one uppercase letter, lowercase letter,
          number, and non-alphabetic character
        </p>
      </form>
      <button
        className="login-button"
        onSubmit={submitForm}
        onClick={submitForm}
      >
        {!isAuth ? (
          submitPending ? (
            <Check color={"#000"} />
          ) : (
            "Register"
          )
        ) : (
          <Navigate to="/app" />
        )}
      </button>
      <p style={{ color: "white" }}>
        {isRegisteredError === "" ? "" : isRegisteredError}
      </p>
      <Link to="/login" className="white-link">
        Already registered? Sign-in
      </Link>
    </div>
  )
}

export const Register = () => {
  return (
    <>
      <BackToHome />
      <RegisterForm />
    </>
  )
}
