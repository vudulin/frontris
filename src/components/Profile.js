import React from "react"
import { useSelector } from "react-redux"

import { BackToHome } from "./"
import "../styles/Profile.scss"

export const Profile = () => {
  const userName = useSelector((state) => state.userData.userName)

  return (
    <div className="main" style={{ color: "white", cursor: "pointer" }}>
      <BackToHome />
      <div>{userName}'s profile Page</div>
    </div>
  )
}
