import "../styles/Circle.css"
import React from "react"
import { useSelector } from "react-redux"

import blocks from "../createStore/blocks"
import { Cell } from "./"

export const Circle = () => {
  const next = useSelector((state) => state.gridState.next)
  const block = next === null ? [[]] : blocks[next]

  const gridStyle = {
    gridTemplateRows: `repeat(${block.length}, 25px)`,
    gridTemplateColumns: `repeat(${block[0].length}, 25px)`,
  }
  const cellStyle = {
    border: "3px white solid",
  }

  return (
    <div className="circle">
      <div className="circleGrid" style={gridStyle}>
        {block.map((r) =>
          r.map((c, i) => <Cell key={"next " + i} data={c} style={cellStyle} />)
        )}
      </div>
    </div>
  )
}
