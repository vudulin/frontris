import React, { useState } from "react"
import axios from "axios"
import { Link, Navigate } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"

import { setIsAuth } from "../createStore/reducers/authSlice"
import { setUserData } from "../createStore/reducers/userDataSlice"
import { setToken } from "../createStore/reducers/tokenSlice"

import { BackToHome } from "./"
import "../styles/LoginForm.scss"

const Check = ({ color }) => {
  return (
    <svg
      className="spinner"
      width="30px"
      height="30px"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
    >
      <rect
        className="spinner__rect"
        x="0"
        y="0"
        width="100"
        height="100"
        fill="none"
      ></rect>
      <circle
        className="spinner__circle"
        cx="50"
        cy="50"
        r="40"
        stroke={color}
        fill="none"
        strokeWidth="6"
        strokeLinecap="round"
      ></circle>
    </svg>
  )
}

const LoginForm = () => {
  const [validData, setValidData] = useState({ email: "", password: "" })
  const [inputData, setInputData] = useState({ email: "", password: "" })
  const [isEmailError, setIsEmailError] = useState(false)
  const [isPasswordError, setIsPasswordError] = useState(false)

  const isAuth = useSelector((state) => state.auth.isAuth)
  const [submitPending, setSubmitPending] = useState(null)

  const dispatch = useDispatch()
  const setIsNotError = (name) => {
    if (name === "email") setIsEmailError((state) => (state = false))
    if (name === "password") setIsPasswordError((state) => (state = false))
  }
  const setIsError = (name) => {
    if (name === "email") setIsEmailError((state) => (state = true))
    if (name === "password") setIsPasswordError((state) => (state = true))
  }
  const handleChangeData = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    const string = name === "email" ? value.toLowerCase() : value
    setInputData((state) => {
      return {
        ...state,
        [name]: string,
      }
    })
    if (validate(name, string)) {
      setValidData((state) => {
        return {
          ...state,
          [name]: string,
        }
      })
      setIsNotError(name)
    }
    if (validData[name] !== ("" && inputData[name])) validData[name] = ""
  }

  const validate = (typeOfData, input) => {
    const validationRegex = {
      email:
        /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/,
      password: /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/,
    }
    return validationRegex[typeOfData].test(input)
  }

  const checkOnFocus = (e) => {
    const { name, value } = e.target
    value === "" && inputData[name] === ""
      ? setIsNotError(name)
      : !validate(name, inputData[name]) && setIsError(name)
  }

  const checkOnBlur = (e) => {
    setIsNotError(e.target.name)
  }

  const submitForm = (e) => {
    e.preventDefault()
    setSubmitPending(true)
    checkLoginData()
  }

  const checkLoginData = async () => {
    const { email, password } = validData
    await axios
      .post("https://backtris.vercel.app/auth/login", { email, password })
      .then((res) => {
        if (res) {
          setSubmitPending(false)
          dispatch(setIsAuth(true))
          dispatch(setUserData(res.data.userData))
          dispatch(setToken(res.data.token))
        }
      })
      .catch((error) => console.log("error - ", error.message))
  }

  const onKeyDownEnter = (e) => {
    const { email, password } = validData
    if (e.key.toLowerCase() === "enter") {
      const form = e.target.form
      const index = [...form].indexOf(e.target)
      email !== "" && password !== "" && submitForm(e)
      if (email && password) return
      index === form.elements.length - 1
        ? form.elements[0].focus()
        : form.elements[index + 1].focus()
    }
    return
  }

  return (
    <div className="form">
      <form className="login_form" onKeyDown={onKeyDownEnter}>
        <p className="login__title">Enter your email and password</p>
        <input
          className="login-input"
          type="email"
          required=""
          placeholder="email"
          name="email"
          onFocus={checkOnFocus}
          onBlur={checkOnBlur}
          onChange={handleChangeData}
        />
        <p
          className="tooltip"
          style={isEmailError ? { opacity: 1 } : { opacity: 0 }}
        >
          Please enter a valid email address, e.g. user@user.com
        </p>
        <input
          className="login-input"
          type="password"
          required=""
          placeholder="password"
          name="password"
          onFocus={checkOnFocus}
          onBlur={checkOnBlur}
          onChange={handleChangeData}
        />
        <p
          className="tooltip"
          style={isPasswordError ? { opacity: 1 } : { opacity: 0 }}
        >
          Your password must be between 8 and 16 characters long, with no
          spaces, and contain at least one uppercase letter, lowercase letter,
          number, and non-alphabetic character
        </p>
      </form>
      <button
        className="login-button"
        onSubmit={submitForm}
        onClick={submitForm}
      >
        {!isAuth ? (
          submitPending ? (
            <Check color={"#000"} />
          ) : (
            "Login"
          )
        ) : (
          <Navigate to="/app" />
        )}
      </button>
      <Link to="/register" className="white-link">
        Haven't registered yet? Please, sign up here
      </Link>
    </div>
  )
}

export const Login = () => {
  return (
    <>
      <BackToHome />
      <LoginForm />
    </>
  )
}
