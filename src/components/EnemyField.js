import "../styles/PlayField.css"

import { Cell } from "./"

export const EnemyField = (props) => {
  const { gridState } = props

  return <div className="play-field">{buildGrid(gridState)}</div>
}

function buildGrid(grid) {
  return grid.map((r, i) => {
    const row = r.map((c, j) => {
      return <Cell key={i * 10 + j + 220 * 1} data={c} />
    })
    return row
  })
}
