import React from "react"
// import { Link } from "react-router-dom"

export const MainLayout = () => {
  return (
    <>
      <div className="wrapper">
        <header className="header">
          <div className="sq1">
            <div className="sq"></div>
            <div className="sq"></div>
            <div className="sq2">
              <div className="sq12"></div>
              <div className="sq12"></div>
            </div>
          </div>
          <div className="sq123">
            <div className="sq1-1"></div>
            <div className="sq1-1"></div>
          </div>
          <div className="sq11">
            <div className="sq1-2"></div>
            <div className="sq1-2"></div>
          </div>
          <div className="sqsq">
            <div className="sqsq1"></div>
            <div className="sqsq1"></div>
            <div className="sqsq1"></div>
            <div className="sqsq1"></div>
          </div>
          <div className="sqsq1234">
            <div className="sqsq12"></div>
            <div className="sqsq12"></div>
            <div className="sqsq12"></div>
            <div className="sqsq12"></div>
          </div>
          <div className="sqsq122"></div>
          <div className="tetion">
            <div className="circle">
              <div className="mainflex">
                <div className="flexk">
                  <div className="kvhj" id="kv1"></div>
                  <div className="kvhj" id="kv2"></div>
                </div>
                <div className="flexx">
                  <div className="kvhj" id="kv3"></div>
                  <div className="kvhj" id="kv4"></div>
                </div>
              </div>
            </div>
            <p className="p1"> Tetion</p>
          </div>
        </header>
        <main className="main">
          <div className="sqq">
            <div className="sq_q"></div>
            <div className="sq_q"></div>
          </div>
          <div className="sqqq">
            <div className="sq_qq"></div>
            <div className="sq_qq"></div>
          </div>
          <div className="sqqqs">
            <div className="sq_qqs"></div>
            <div className="sq_qqs"></div>
            <div className="sq_qqs"></div>
          </div>
          <div className="sq_qqs1"></div>
          <div className="sqqqs12">
            <div className="sq_qqs12"></div>
            <div className="sq_qqs12"></div>
          </div>
          <div className="sqqqs123">
            <div className="sq_qqs123"></div>
            <div className="sq_qqs123"></div>
          </div>
          <div className="sq_qqs12356"></div>
          <div className="sq_qqs1235"></div>
          <div className="sqqqs1234">
            <div className="sq_qqs1234"></div>
            <div className="sq_qqs1234"></div>
          </div>
          <div className="cont">
            <div className="sqqqs12345">
              <div className="sq_qqs12345"></div>
              <div className="sq_qqs12345"></div>
            </div>
            <div className="sqqqs123456">
              <div className="sq_qqs123456"></div>
              <div className="sq_qqs123456"></div>
            </div>
          </div>
        </main>
        <footer className="footer"></footer>
      </div>
    </>
  )
}
