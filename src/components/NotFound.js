import React from "react"
import "../styles/NotFound.scss"

export const NotFound = () => {
  return (
    <div className="nf-wrapper">
      <img src="404.jpg" alt="The truth is over there" className="nf-image" />
      {/* The truth is over there */}
    </div>
  )
}
