import React from "react"
import "../styles/LeftField.css"

import { InfoField, PlayerField } from "./"

export const LeftField = () => {
  return (
    <div className="left-field">
      <InfoField />
      <PlayerField />
    </div>
  )
}
