import React from "react"
import { Link } from "react-router-dom"

export const BackToHome = () => {
  return (
    <div
      className="signin-links"
      style={{
        display: "flex",
        alignSelf: "end",
        width: "100%",
        font: "1.5em sans-serif",
      }}
    >
      <Link to="/" className="white-link">
        Back to Home
      </Link>
    </div>
  )
}
