// Change other components to named export, not default
export { Home } from "./Home"
export { Avatar } from "./Avatar"
export { Cell } from "./Cell"
export { Circle } from "./Circle"
export { EnemyField } from "./EnemyField"
export { InfoField } from "./InfoField"
export { LeftField } from "./LeftField"
export { Login } from "./Login"
export { PlayerField } from "./PlayerField"
export { Register } from "./Register"
export { Profile } from "./Profile"
export { BackToHome } from "./BackToHome"
export { MainLayout } from "./MainLayout"
export { RightField } from "./RightField"
export { NotFound } from "./NotFound"
