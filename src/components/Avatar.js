import "../styles/Avatar.css"
import React from "react"
import { useSelector } from "react-redux"

export const Avatar = () => {
  const { userName, avatar } = useSelector((state) => state.userData)

  return (
    <div className="avatar">
      <img
        className="avatar avatar__img"
        src={avatar || "/player-1.jpg"}
        alt="avatar"
      />
      <p className="avatar avatar__name">{userName || "Player"}</p>
    </div>
  )
}
