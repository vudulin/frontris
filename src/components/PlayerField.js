import React, {
  useEffect,
  useState,
  useLayoutEffect,
  useRef,
  useCallback,
} from "react"
import { useDispatch, useSelector } from "react-redux"
import {
  setNext,
  setPoints,
  setIsPlaying,
  setIsGameOver,
  setNewGame,
} from "../createStore/reducers/gridSlice"
import { Cell } from "./"
import { blockStateCheck, keyCheck } from "../operations"
import "../styles/PlayField.css"

export function PlayerField(props) {
  const initialGrid = new Array(22).fill(new Array(10).fill(0))
  const { next, points, isPlaying, isGameOver, newGame } = useSelector(
    (state) => state.gridState
  )
  const [grid, setGrid] = useState(initialGrid)
  const [isBlock, setIsBlock] = useState(false)
  const [currentLoc, setCurrentLoc] = useState([3, 0])
  const [pattern, setPattern] = useState(null)
  const [ticker, setTicker] = useState(false)
  const [speed, setSpeed] = useState(700)
  const block = {
    currentLoc: currentLoc,
    isBlock: isBlock,
    pattern: pattern,
    next: next,
  }
  const dispatch = useDispatch()

  const keyPress = useEvent((e) => {
    if (e.key === " ") {
      dispatch(setIsPlaying(!isPlaying))
    }
    if (!isPlaying || isGameOver) return
    if (keyCheck(e.key)) {
      gridRender(grid, block, e.key)
    }
  })

  useEffect(() => {
    window.addEventListener("keydown", keyPress)
    return () => {
      window.removeEventListener("keydown", keyPress)
    }
  }, [keyPress])

  useEffect(() => {
    if (newGame) {
      setGrid(initialGrid)
      setSpeed(700)
      setPattern(null)
      setIsBlock(false)
      // dispatch(setNext(null))
      dispatch(setPoints(0))
      dispatch(setNewGame(false))
    }
    if (points >= 5000) setSpeed(500)
    if (points >= 10000) setSpeed(300)
    if (points >= 15000) setSpeed(150)
    if (!isPlaying || isGameOver) return
    let tick = setTimeout(() => {
      setTicker(!ticker)
      window.dispatchEvent(new KeyboardEvent("keydown", { key: "ArrowDown" }))
    }, speed)
    return function () {
      clearTimeout(tick)
    }
  }, [ticker, speed, isPlaying, isGameOver, newGame, points])

  return <div className="play-field">{buildGrid(grid)}</div>

  function gridRender(grid, block, key) {
    const [newGrid, newBlock, countLines] = blockStateCheck(grid, block, key)

    if (newGrid[0][0] === -3) dispatch(setIsGameOver(true))
    setGrid(newGrid)
    setIsBlock(newBlock.isBlock)
    setCurrentLoc(newBlock.currentLoc)
    setPattern(newBlock.pattern)
    dispatch(setNext(newBlock.next))
    if (countLines) dispatch(setPoints(points + countPoints(countLines)))
  }
}

function buildGrid(grid) {
  return grid.map((r, i) => {
    const row = r.map((c, j) => {
      return <Cell key={i * 10 + j + 220} data={c} />
    })
    return row
  })
}

function countPoints(countLines) {
  switch (countLines) {
    case 1:
      return 100
    case 2:
      return 300
    case 3:
      return 700
    case 4:
      return 1500
    default:
      return 0
  }
}

function useEvent(handler) {
  const handlerRef = useRef(null)

  useLayoutEffect(() => {
    handlerRef.current = handler
  })

  return useCallback((...args) => {
    const fn = handlerRef.current
    return fn(...args)
  }, [])
}
