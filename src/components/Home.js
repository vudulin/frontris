import React from "react"
import { Link } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { setIsAuth } from "../createStore/reducers/authSlice"

import "../styles/Home.scss"

const LoginIcon = () => {
  return (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="30px"
      y="30px"
      stroke="white"
      fill="white"
      viewBox="0 0 512 512"
      space="preserve"
    >
      <g>
        <g>
          <path
            d="M327.287,246.852l-74.931-76.595c-4.941-5.06-13.039-5.146-18.099-0.205c-5.06,4.941-5.146,13.056-0.205,18.099
        l53.854,55.057H12.8C5.734,243.2,0,248.934,0,256c0,7.066,5.734,12.8,12.8,12.8h275.098l-53.845,55.057
        c-4.941,5.043-4.855,13.158,0.205,18.099c5.06,4.941,13.158,4.855,18.099-0.205l75.128-76.8
        C332.424,259.908,332.339,251.793,327.287,246.852z"
          />
        </g>
      </g>
      <g>
        <g>
          <path
            d="M499.2,0H166.4c-7.066,0-12.8,5.734-12.8,12.8V192h25.6V25.6h307.2v460.8H179.2V320h-25.6v179.2
        c0,7.066,5.734,12.8,12.8,12.8h332.8c7.066,0,12.8-5.734,12.8-12.8V12.8C512,5.734,506.266,0,499.2,0z"
          />
        </g>
      </g>
    </svg>
  )
}
const LogoutIcon = () => {
  return (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="30px"
      y="30px"
      stroke="white"
      fill="white"
      viewBox="0 0 512 512"
      space="preserve"
    >
      <g>
        <g>
          <path
            d="M358.401,192V12.8c0-7.066-5.734-12.8-12.8-12.8h-332.8c-7.066,0-12.8,5.734-12.8,12.8v486.4
			c0,7.066,5.734,12.8,12.8,12.8h332.8c7.066,0,12.8-5.734,12.8-12.8V320h-25.6v166.4h-307.2V25.6h307.2V192H358.401z"
          />
        </g>
      </g>
      <g>
        <g>
          <path
            d="M508.161,246.844l-74.931-76.595c-4.941-5.06-13.047-5.146-18.099-0.205c-5.052,4.941-5.146,13.056-0.205,18.099
			l53.854,55.057H193.673c-7.066,0-12.8,5.734-12.8,12.8c0,7.066,5.734,12.8,12.8,12.8h275.098l-53.854,55.057
			c-4.941,5.043-4.847,13.158,0.205,18.099c5.06,4.941,13.158,4.855,18.099-0.205l75.128-76.8
			C513.289,259.9,513.204,251.785,508.161,246.844z"
          />
        </g>
      </g>
    </svg>
  )
}
export const Home = () => {
  const isAuth = useSelector((state) => state.auth.isAuth)
  const { userName, avatar } = useSelector((state) => state.userData)

  const dispatch = useDispatch()

  const setIsNotAuth = () => {
    if (isAuth) dispatch(setIsAuth(!isAuth))
  }
  return (
    <div className="wrapper" /* onLoad={checkIsID} */>
      <div className="signin-links">
        {isAuth ? (
          <>
            <div className="user-logo">
              <Link to="/profile" className="white-link">
                <p style={{ textAlign: "center" }}>{userName}</p>
                <img src={avatar || "/player-1.jpg"} alt="avatar" />
              </Link>
            </div>
            <div
              onClick={setIsNotAuth}
              style={{ color: "white", cursor: "pointer" }}
            >
              <LogoutIcon />
            </div>
          </>
        ) : (
          <>
            <Link to="/login" className="white-link">
              <LoginIcon />
            </Link>
          </>
        )}
      </div>
      <div className="main">
        <div style={{ cursor: "default" }}>TETRIS</div>
        <button className="start-button">
          <Link to={isAuth ? "/app" : "/login"} className="black-link">
            Let's play
          </Link>
        </button>
      </div>
    </div>
  )
}
