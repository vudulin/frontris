import "../styles/Cell.css"
import React, { memo } from "react"

const CellComponent = (props) => {
  const color = (data) => {
    switch (data) {
      case 0:
        return { backgroundColor: "black" }
      case 1:
        return { backgroundColor: "darkblue" }
      case 2:
        return { backgroundColor: "yellow" }
      case 3:
        return { backgroundColor: "blue" }
      case 4:
        return { backgroundColor: "green" }
      case 5:
        return { backgroundColor: "purple" }
      case 6:
        return { backgroundColor: "orange" }
      case 7:
        return { backgroundColor: "darkred" }
      case 11:
        return { backgroundColor: "red" }
      case 12:
        return { backgroundColor: "red" }
      case 13:
        return { backgroundColor: "red" }
      case 14:
        return { backgroundColor: "red" }
      case 15:
        return { backgroundColor: "red" }
      case 16:
        return { backgroundColor: "red" }
      case 17:
        return { backgroundColor: "red" }

      default:
        return { backgroundColor: "black", border: "black" }
    }
  }

  return <div className="cell" style={color(props.data)}></div>
}

export const Cell = memo(CellComponent)
