import React from "react"
import "../styles/RightField.css"

import { InfoField, PlayerField } from "./"

export const RightField = (props) => {
  return (
    <div className="right-field">
      <InfoField />
      <PlayerField />
    </div>
  )
}
