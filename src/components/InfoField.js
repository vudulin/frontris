import "../styles/InfoField.css"
import React from "react"
import { useSelector } from "react-redux"

import { Avatar, Circle } from "./"

export const InfoField = () => {
  const points = useSelector((state) => state.gridState.points)
  return (
    <div className="info-field">
      <div className="nextFigure">
        <Circle />
        <p className="title">NEXT</p>
      </div>
      <div className="score">
        <p className="points">Points</p>
        <p className="score-points">{points}</p>
      </div>
      <Avatar playerId={2} />
    </div>
  )
}
