## Online Tetris App - Tetion

This game allows you to compete with other users online.

The opponent's playing field is displayed on the right, which allows you to follow the progress of the game and opponent's statistics.

You can save your profile, settings and rating, watch your playing history.

To start the game press "Enter" or down arrow key "ArrowDown". Rotate the figure block with key "A". Move it left, right or down with arrow keys "ArrowLeft", "ArrowRight" and "ArrowDown" respectively. Matching whole one line gives you 100 points, two lines in a row - 300 points, three lines - 700 points, and four lines - 1500 points


#### Technologies
This app is developed with React, Redux, Typescript, SCSS on frontend and with Express, Typescript on backend site
